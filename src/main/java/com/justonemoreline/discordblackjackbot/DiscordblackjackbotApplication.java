package com.justonemoreline.discordblackjackbot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DiscordblackjackbotApplication {

	public static void main(String[] args) {
		SpringApplication.run(DiscordblackjackbotApplication.class, args);
	}

}
